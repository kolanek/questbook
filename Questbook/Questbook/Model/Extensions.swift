//
//  Extensions.swift
//  Questbook
//
//  Created by Bartosz Kolanek on 09.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UITextField{
    func placeHolder(placeHolder: String){
        if self.text == "" {
            self.text = placeHolder
        }else if self.text == placeHolder{
            self.text = ""
        }
    }
}

extension UITextView{
    func placeHolder(placeHolder: String){
        if self.text == "" {
            self.text = placeHolder
        }else if self.text == placeHolder{
            self.text = ""
        }
    }
}
