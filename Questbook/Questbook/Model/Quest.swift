//
//  Quest.swift
//  Questbook
//
//  Created by Bartosz Kolanek on 02.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

@objc enum QuestImportance: Int{
    case epic
    case primary
    case secondary
    
    var color: UIColor {
        switch self {
        case .epic: return Colors.importanceOrange.tint
        case .primary: return Colors.importanceBlue.tint
        case .secondary: return Colors.importanceGreen.tint
        }
    }
    
    var expiriencePoints: Int {
        switch self {
        case .epic:
            return 150000
        case .primary:
            return 5000
        case .secondary:
            return 500
        }
    }
    
    var toString: String {
        switch self {
        case .epic:
            return "Epic"
        case .primary:
            return "Primary"
        case .secondary:
            return "Secondary"
        }
    }
    
    static func allValues() -> [QuestImportance]{
        return [.secondary, .primary, .epic]
    }
}

@objc enum QuestType: Int {
    case dailyProgress
    case normal
}

@objc enum QuestState: Int {
    case notCompleted
    case readyToComplete
    case completed
    case abandoned
}

@objcMembers class Quest: Object{
    
    dynamic var questID = UUID().uuidString
    dynamic var questName: String = ""
    dynamic var questImportance: QuestImportance = .secondary
    dynamic var questType: QuestType = .normal
    dynamic var questEP: Int = 0
    dynamic var questDeadline: Date? = nil
    dynamic var questDescription: String? = nil
    let subQuestIDs = List<String>()
    let subQuests = RealmSwift.List<Quest>()
    dynamic var questState: QuestState = .notCompleted
    dynamic var questProgressCap: Int = 0
    dynamic var questProgress: Int = 0
    dynamic var lastProgress: Date? = nil
    
    convenience init(name: String, importance: QuestImportance, type: QuestType, deadline: Date?, description: String?, subquestsIDs: [String], progresCap: Int) {
        self.init()
        self.questName = name
        self.questImportance = importance
        self.questType = type
        self.questEP = questImportance.expiriencePoints
        self.questDeadline = deadline
        self.questProgressCap = progresCap
        self.questProgress = 0
        self.lastProgress = Date()
        self.questDescription = description
        self.questState = .notCompleted
        for id in subquestsIDs {
            self.subQuestIDs.append(id)
        }
        if subQuestIDs.isEmpty && questType == .normal {
            readyToComplete()
        }
    }
    
    func chainQuestIsReadyToComplete(subquests: [Quest]) -> Bool {
        if !subquests.isEmpty {
            for subquest in subquests {
                if subquest.questState != .completed{
                    return false
                }
            }
            readyToComplete()
            return true
        }else{
            return false
        }
    }
    
    func readyToComplete(){
        if questState == .notCompleted{
            let dict: [String: Any?] = ["questState": QuestState.readyToComplete.hashValue]
            RealmService.shared.update(self, with: dict)
        }
    }
    
    func complete(){
        if questState == .readyToComplete{
            let dict: [String: Any?] = ["questState": QuestState.completed.hashValue]
            RealmService.shared.update(self, with: dict)
        }
    }
    
    func abandon(){
        let dict: [String: Any?] = ["questState": QuestState.abandoned.hashValue]
        RealmService.shared.update(self, with: dict)
    }
    
    func progressQuest(){
        switch questState {
        case .notCompleted:
            let dict: [String: Any?] = ["questProgress": questProgress+1, "lastProgress": Date()]
            RealmService.shared.update(self, with: dict)
            if(questProgress==questProgressCap){
                readyToComplete()
            }
        case .readyToComplete:
            let dict: [String: Any?] = ["questProgress": questProgress+1, "lastProgress": Date()]
            RealmService.shared.update(self, with: dict)
            complete()
        case .completed:
            break
        case .abandoned:
            break
        }
    }
    
}
