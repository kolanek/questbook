//
//  Achievement.swift
//  Questbook
//
//  Created by Bartosz Kolanek on 18.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

@objcMembers class Achievement: Object {
    dynamic var id: Int = 0
    dynamic var name: String = ""
    dynamic var descript: String = ""
    dynamic var completed: Bool = false
    
    convenience init(id: Int, name: String, descript: String) {
        self.init()
        self.id = id
        self.name = name
        self.descript = descript
    }
    
    func complete(){
        let dict: [String: Any?] = ["completed": true]
        RealmService.shared.update(self, with: dict)
    }
    
}
