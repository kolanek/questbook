//
//  ApplicationFeatures.swift
//  Questbook
//
//  Created by Bartosz Kolanek on 02.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import Foundation
import UIKit

enum AppFeatures {
    case quests
    case achievements
    case checklists
    
    static var count = 3
    
    var icon: UIImage {
        switch self {
        case .quests: return UIImage(named: "IconQuest")!
        case .achievements: return UIImage(named: "IconAchievement")!
        case .checklists: return UIImage(named: "IconChecklist")!
        }
    }
    
    var label: String {
        switch self {
        case .quests: return "Quests"
        case .achievements: return "Achievements"
        case .checklists: return "Checklists"
        }
    }
    
    static func accessByHashValue(hashValue: Int) -> AppFeatures{
        switch hashValue {
        case 0:
            return .quests
        case 1:
            return .achievements
        case 2:
            return .checklists
        default:
            return .quests
        }
    }
}



