//
//  Colors.swift
//  Questbook
//
//  Created by Bartosz Kolanek on 05.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import Foundation
import UIKit

enum Colors{
    case abandonRed
    case completedGray
    case importanceOrange
    case importanceBlue
    case importanceGreen
    
    var tint: UIColor {
        switch self {
        case .abandonRed:
            return UIColor(red: 186/255, green: 46/255, blue: 67/255, alpha: 1)
        case .completedGray:
            return UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1)
        case .importanceBlue:
            return UIColor(red: 109/255, green: 158/255, blue: 238/255, alpha: 1)
        case .importanceGreen:
            return UIColor(red: 0, green: 190/255, blue: 124/255, alpha: 1)
        case .importanceOrange:
            return UIColor(red: 219/255, green: 170/255, blue: 19/255, alpha: 1)
        }
    }
}
