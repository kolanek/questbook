//
//  Player.swift
//  Questbook
//
//  Created by Bartosz Kolanek on 16.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import Foundation
import RealmSwift


@objc enum ExpCap: Int{
    case firstCap = 500
    case secondCap = 20000
    case thirdCap = 50000
    case fourthCap = 100000
    case fifthCap = 200000
    case sixthCap = 400000
    case seventhCap = 800000
    case eighthCap = 1600000
}

@objc enum PlayerRank: Int {
    case rookie
    case beginner
    case intermediate
    case advanced
    case proffessional
    case master
    case grandmaster
    case lifewinner
    
    var toString: String{
        switch self {
        case .rookie:
            return "Rookie"
        case .beginner:
            return "Beginner"
        case .intermediate:
            return "Intermediate"
        case .advanced:
            return "Advanced"
        case .proffessional:
            return "Professional"
        case .master:
            return "Master"
        case .grandmaster:
            return "Grand Master"
        case .lifewinner:
            return "Life Winner"
        }
    }
    
}

@objcMembers class Player: Object {
    dynamic var exp = 0
    dynamic var rank: PlayerRank = .rookie
    dynamic var level: Int = 1
    dynamic var secondaryQuestsCompleted: Int = 0
    dynamic var primaryQuestsCompleted: Int = 0
    dynamic var epicQuestsCompleted: Int = 0
    dynamic var questsCompleted: Int = 0
    
    dynamic var secondaryQuestsAbandoned: Int = 0
    dynamic var primaryQuestsAbandoned: Int = 0
    dynamic var epicQuestsAbandoned: Int = 0
    dynamic var questsAbandoned: Int = 0
    
    convenience init(name: String){
        self.init()
        exp = 0
        rank = .rookie
        level = 1
    }
    
    func addEXP(points: Int){
        let currentEXP = exp
        let dict: [String: Any?] = ["exp": currentEXP + points]
        RealmService.shared.update(self, with: dict)
        setLevel()
    }
    
    func questAbandoned(quest: Quest){
        switch quest.questImportance {
        case .secondary:
            updateQuestCounter(name: "secondaryQuestsAbandoned", value: secondaryQuestsAbandoned+1)
        case .primary:
            updateQuestCounter(name: "primaryQuestsAbandoned", value: primaryQuestsAbandoned+1)
        case .epic:
            updateQuestCounter(name: "epicQuestsAbandoned", value: epicQuestsAbandoned+1)
        }
        updateQuestCounter(name: "questsAbandoned", value: secondaryQuestsAbandoned+primaryQuestsAbandoned+epicQuestsAbandoned)
        addEXP(points: -quest.questEP/2)
    }
    
    func questCompleted(quest: Quest){
        switch quest.questImportance {
        case .secondary:
            updateQuestCounter(name: "secondaryQuestsCompleted", value: secondaryQuestsCompleted+1)
        case .primary:
            updateQuestCounter(name: "primaryQuestsCompleted", value: primaryQuestsCompleted+1)
        case .epic:
            updateQuestCounter(name: "epicQuestsCompleted", value: epicQuestsCompleted+1)
        }
        updateQuestCounter(name: "questsCompleted", value: secondaryQuestsCompleted+primaryQuestsCompleted+epicQuestsCompleted)
        addEXP(points: quest.questEP)
    }
    
    
    func updateQuestCounter(name: String, value: Int){
        let dict: [String: Any?] = [name: value]
        RealmService.shared.update(self, with: dict)
    }
    
    func setLevel(){
        
        let currentLevel = level
        var newLevel: Int
        var newRank: PlayerRank
        
        switch exp {
        case let x where x<ExpCap.firstCap.rawValue:
            newLevel = 1
            newRank = .rookie
        case let x where x>=ExpCap.firstCap.rawValue && x<ExpCap.secondCap.rawValue:
            newLevel = 2
            newRank = .beginner
        case let x where x>=ExpCap.secondCap.rawValue && x<ExpCap.thirdCap.rawValue:
            newLevel = 3
            newRank = .intermediate
        case let x where x>=ExpCap.thirdCap.rawValue && x<ExpCap.fourthCap.rawValue:
            newLevel = 4
            newRank = .advanced
        case let x where x>=ExpCap.fourthCap.rawValue && x<ExpCap.fifthCap.rawValue:
            newLevel = 5
            newRank = .proffessional
        case let x where x>=ExpCap.fifthCap.rawValue && x<ExpCap.sixthCap.rawValue:
            newLevel = 6
            newRank = .master
        case let x where x>=ExpCap.sixthCap.rawValue && x<ExpCap.seventhCap.rawValue:
            newLevel = 7
            newRank = .grandmaster
        case let x where x>=ExpCap.seventhCap.rawValue && x<ExpCap.eighthCap.rawValue:
            newLevel = 8
            newRank = .lifewinner
        default:
            newLevel = 0
            newRank = .beginner
        }
        
        if currentLevel != newLevel {
            let dict: [String: Any?] = ["level": newLevel, "rank": newRank.hashValue]
            RealmService.shared.update(self, with: dict)
        }
    }
    
    func levelUp(){
        print("levelUp")
    }
}
