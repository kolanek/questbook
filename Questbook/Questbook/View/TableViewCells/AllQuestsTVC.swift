//
//  AllQuestsTVC.swift
//  Questbook
//
//  Created by Bartosz Kolanek on 02.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import UIKit

class AllQuestsTVC: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func confifure(name: String, color: UIColor){
        label.text = name
        label.textColor = color
    }
    
}
