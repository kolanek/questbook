//
//  MainScreenTVC.swift
//  Questbook
//
//  Created by Bartosz Kolanek on 02.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import UIKit

class MainScreenTVC: UITableViewCell {

    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(image: UIImage, name: String){
        icon.image = image
        label.text = name
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
