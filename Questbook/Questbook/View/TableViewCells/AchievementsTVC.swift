//
//  AchievementsTVC.swift
//  Questbook
//
//  Created by Bartosz Kolanek on 18.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import UIKit

class AchievementsTVC: UITableViewCell {

    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptLabel: UILabel!
    @IBOutlet weak var view: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func confifure(achievement: Achievement){
        nameLabel.text = achievement.name
        descriptLabel.text = achievement.descript
        if !achievement.completed {
            view.alpha = 0.4
        }
    }
    
}
