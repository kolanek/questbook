//
//  AddQuestViewController.swift
//  Questbook
//
//  Created by Bartosz Kolanek on 06.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import UIKit
import RealmSwift

class AddQuestViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var subquestView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var questImportanceButton: UIButton!
    @IBOutlet weak var questDeadlineButton: UIButton!
    @IBOutlet weak var addQuestButton: UIButton!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var dailyProgressLabel: UILabel!
    @IBOutlet weak var dailyProgressSwitch: UISwitch!
    @IBOutlet weak var tableView: UITableView!
    
    var questName: String = ""
    var questDescription: String = ""
    var questImportance: QuestImportance = .secondary
    var questType: QuestType = .normal
    var questDeadline: Date? = nil
    var subQuests: [Quest] = []
    var subQuestsIDs: [String] = []
    var progressCap: Int = 0
    
    var questNamePlaceholder = "Quest Name"
    var questDescriptionPlaceholder = "Quest description"
    
    var quests: Results<Quest>!
    var notificationToken: NotificationToken?
    
    
    @IBAction func importanceSelectionClicked(sender: UIButton){
        let questImportanceValues = QuestImportance.allValues()
        let index = questImportanceValues.index(of: questImportance)
        if (index! < questImportanceValues.count-1){
            questImportance = questImportanceValues[index!+1]
        }else{
            questImportance = questImportanceValues[0]
        }
        reloadLayout()
        tableView.reloadData()
    }
    
    @IBAction func deadlineSelectionClicked(sender: UIButton){
        if datePickerView.isHidden {
            subquestView.isHidden = true
            datePickerView.isHidden = false
        }else {
            subquestView.isHidden = false
            datePickerView.isHidden = true
        }
    }
    
    
    @IBAction func addQuestClicked(sender: UIButton){
        questName = textField.text!
        questDescription = textView.text!
        
        questDescription == questDescriptionPlaceholder ? questDescription = "" : nil
        
        if questName != questNamePlaceholder {
            if questType == .dailyProgress {
                if questDeadline == nil {
                    let alert = UIAlertController(title: "Set quest deadline", message: "quest deadline must be set when daily progress is selected", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alert, animated: true)
                }else{
                    progressCap = Int(Calendar.current.dateComponents([.day], from: Date(), to: questDeadline!).day!)
                    
                    let newQuest = Quest(name: questName, importance: questImportance, type: questType, deadline: questDeadline, description: questDescription, subquestsIDs: subQuestsIDs, progresCap: progressCap)
                    RealmService.shared.create(newQuest)
                    navigationController?.popViewController(animated: true)
                }
            }else {
                
                let newQuest = Quest(name: questName, importance: questImportance, type: questType, deadline: questDeadline, description: questDescription, subquestsIDs: subQuestsIDs, progresCap: 0)
                RealmService.shared.create(newQuest)
                navigationController?.popViewController(animated: true)
            }
        }else{
            let alert = UIAlertController(title: "Set quest name", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func switchClicked(sender: UISwitch){
        if sender.isOn {
            questType = .dailyProgress
        }else {
            questType = .normal
        }
        reloadLayout()
    }
    
    @IBAction func pickerSelectClicked(sender: UIButton){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM YYYY"
        questDeadline = datePicker.date
        questDeadlineButton.setTitle("Deadline: \(dateFormatter.string(from: questDeadline!))", for: .normal)
        datePickerView.isHidden = true
        subquestView.isHidden = false
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        let realm = RealmService.shared.realm
        quests = realm.objects(Quest.self)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "AllQuestsTVC", bundle: nil), forCellReuseIdentifier: "allQuestsTVC")
        
        textField.delegate = self
        textView.delegate = self
        textField.text = questNamePlaceholder
        textView.text = questDescriptionPlaceholder
        
        datePicker.minimumDate =  Calendar.current.date(byAdding: .day, value: 2, to: Date())
        
        datePickerView.isHidden = true
        tableView.isHidden = true
        reloadLayout()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Subquests
    
    func getAvailableSubquests() -> [Quest]{
        var availableSubquests: [Quest] = []
        var availableSubquestImportnace: QuestImportance? = nil
        questImportance == .epic ? availableSubquestImportnace = .primary : nil
        questImportance == .primary ? availableSubquestImportnace = .secondary : nil
        for quest in quests{
            if availableSubquestImportnace != nil{
                if availableSubquestImportnace == quest.questImportance && quest.questState != QuestState.abandoned && quest.questState != QuestState.completed{
                    availableSubquests.append(quest)
                }
            }
        }
        return availableSubquests
    }
    
    // MARK: - Layout
    
    func reloadLayout(){
        questImportanceButton.setTitle("Importance: \(questImportance.toString)", for: .normal)
        colorLayout(color: questImportance.color)
        if questImportance == .epic {
            dailyProgressSwitch.isEnabled = false
            dailyProgressSwitch.setOn(false, animated: true)
            dailyProgressLabel.alpha = 0.3
        }else {
            dailyProgressSwitch.isEnabled = true
            dailyProgressLabel.alpha = 1
        }
        if questImportance == .secondary {
            tableView.isHidden = true
        }else{
            if dailyProgressSwitch.isOn {
                tableView.isHidden = true
            }else {
                tableView.isHidden = false
            }
        }
    }

    func colorLayout(color: UIColor){
        textField.textColor = color
        dailyProgressLabel.textColor = color
        questImportanceButton.setTitleColor(color, for: .normal)
        questDeadlineButton.setTitleColor(color, for: .normal)
        selectButton.backgroundColor = color
        addQuestButton.backgroundColor = color
        dailyProgressSwitch.onTintColor = color
        self.navigationController?.navigationBar.tintColor = color
    }
    
    // MARK: - TextViews and TextFeilds
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.placeHolder(placeHolder: questNamePlaceholder)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.placeHolder(placeHolder: questNamePlaceholder)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.placeHolder(placeHolder: questNamePlaceholder)
        textView.becomeFirstResponder()
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.placeHolder(placeHolder: questDescriptionPlaceholder)

    }
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.placeHolder(placeHolder: questDescriptionPlaceholder)
    }
    
    // MARK: - TableView
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getAvailableSubquests().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "allQuestsTVC", for: indexPath) as! AllQuestsTVC
    
        
        cell.confifure(name: "\(getAvailableSubquests()[indexPath.row].questName)", color: getAvailableSubquests()[indexPath.row].questImportance.color)
            
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Subquests"
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        subQuests.append(getAvailableSubquests()[indexPath.row])
        subQuestsIDs.append(getAvailableSubquests()[indexPath.row].questID)
        print(subQuests.count)
        print(subQuestsIDs)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let subquestToDeselect = subQuests.filter({$0.questID == getAvailableSubquests()[indexPath.row].questID}).first
        let index = subQuests.index(of: subquestToDeselect!)
        subQuests.remove(at: index!)
        
        let index2 = subQuestsIDs.index(of: getAvailableSubquests()[indexPath.row].questID)
        subQuestsIDs.remove(at: index2!)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
