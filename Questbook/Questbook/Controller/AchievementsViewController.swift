//
//  AchievementsViewController.swift
//  Questbook
//
//  Created by Bartosz Kolanek on 18.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import UIKit
import RealmSwift

class AchievementsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var achievements: Results<Achievement>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let realm = RealmService.shared.realm
        achievements = realm.objects(Achievement.self)

        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "AchievementsTVC", bundle: nil), forCellReuseIdentifier: "achievementsTVC")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return achievements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "achievementsTVC", for: indexPath) as! AchievementsTVC
        
        let achievement = achievements[indexPath.row]
        cell.confifure(achievement: achievement)
        
        return cell
    }
}
