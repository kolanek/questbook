//
//  MainScreenViewController.swift
//  Questbook
//
//  Created by Bartosz Kolanek on 02.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import UIKit
import RealmSwift

class MainScreenViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var rankLabel: UILabel!
    
    var playerResult: Results<Player>!
    var achievementsResult: Results<Achievement>!
    var notificationToken: NotificationToken?
    
    var player: Player = Player()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let realm = RealmService.shared.realm
        playerResult = realm.objects(Player.self)
        achievementsResult = realm.objects(Achievement.self)

        if playerResult.count == 0 {
            RealmService.shared.create(Player())
        }
        if achievementsResult.count == 0{
            createAchievements()
        }
        
        player = playerResult[0]
        print(player)
        
        rankLabel.text = player.rank.toString
        levelLabel.text = "Level \(player.level)"
        
        // Setting up TableView
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "MainScreenTVC", bundle: nil), forCellReuseIdentifier: "mainScreenTVC")
    }

    func createAchievements(){
        RealmService.shared.create(Achievement(id: 1, name: "It's a start", descript: "Complete your first Quest"))
        RealmService.shared.create(Achievement(id: 2, name: "10/10", descript: "Complete 10 quests"))
        RealmService.shared.create(Achievement(id: 3, name: "Quest addict", descript: "Complete 50 quests"))
        RealmService.shared.create(Achievement(id: 4, name: "Serial quester", descript: "Complete 100 quests"))
        RealmService.shared.create(Achievement(id: 5, name: "I'll do it later", descript: "Complete SECONDARY quest"))
        RealmService.shared.create(Achievement(id: 6, name: "That staff matters", descript: "Complete PRIMARY quest"))
        RealmService.shared.create(Achievement(id: 7, name: "Livegoal", descript: "Complete EPIC quest"))
        RealmService.shared.create(Achievement(id: 8, name: "I'll do it later", descript: "Complete secondary quest"))
        RealmService.shared.create(Achievement(id: 9, name: "Beginner", descript: "Get level 2"))
        RealmService.shared.create(Achievement(id: 10, name: "Advanced", descript: "Get level 3"))
        RealmService.shared.create(Achievement(id: 11, name: "Intermediate", descript: "Get level 4"))
        RealmService.shared.create(Achievement(id: 12, name: "Professional", descript: "Get level 5"))
        RealmService.shared.create(Achievement(id: 13, name: "Master", descript: "Get level 6"))
        RealmService.shared.create(Achievement(id: 14, name: "Grand master", descript: "Get level 7"))
        RealmService.shared.create(Achievement(id: 15, name: "Live winner", descript: "Get level 8"))
        RealmService.shared.create(Achievement(id: 16, name: "Daily Rally", descript: "Complete DAILY PROGRESS quest"))

    }
    
    
    // MARK: - TableView Things
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppFeatures.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mainScreenTVC", for: indexPath) as! MainScreenTVC
        
        let feature = AppFeatures.accessByHashValue(hashValue: indexPath.row)
        cell.configure(image: feature.icon, name: feature.label)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            performSegue(withIdentifier: "goToQuests", sender: self)
        case 1:
            performSegue(withIdentifier: "goToAchievements", sender: self)
        case 2:
            let alert = UIAlertController(title: "Not ready yet", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
        default:
            break
        }
    }
}
