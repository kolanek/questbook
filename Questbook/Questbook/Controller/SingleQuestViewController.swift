//
//  SingleQuestViewController.swift
//  Questbook
//
//  Created by Bartosz Kolanek on 03.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import UIKit
import AVFoundation
import RealmSwift

class SingleQuestViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK: - Outlets for quest finished
    
    @IBOutlet weak var questFinishedView: UIView!
    @IBOutlet weak var finishedViewQuestNameLabel: UILabel!
    @IBOutlet weak var questFinishStatusLabel: UILabel!
    @IBOutlet weak var finishedViewExpiriencePointsLabel: UILabel!
    
    
    //MARK: - Outlets for quest completed animation
    
    @IBOutlet weak var questCompletedView: UIView!
    @IBOutlet weak var questFinishedIcon: UIImageView!
    @IBOutlet weak var questFinishedLabel: UILabel!
    
    //MARK: - Outlets and Actions for not completed quest
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var questNotCompletedView: UIView!
    @IBOutlet weak var dailyProgressQuestTypeView: UIView!
    @IBOutlet weak var normalQuestTypeView: UIView!
    
    @IBOutlet weak var notCompletedViewBigQuestNameLabel: UILabel!
    
    @IBOutlet weak var notCompletedViewQuestNameLabel: UILabel!
    @IBOutlet weak var questDescriptionLabel: UILabel!
    
    @IBOutlet weak var completeQuestButton: UIButton!
    @IBOutlet weak var abandonQuestButton: UIButton!
    
    @IBOutlet weak var expiriencePointsToGainLabel: UILabel!
    @IBOutlet weak var expiriencePointsToLooseLabel: UILabel!
    
    @IBOutlet weak var progressLabel: UILabel!
    
    //MARK: - Variables and Constans
    
    var questImportanceColor: UIColor!
    var gloriousSound = AVAudioPlayer()
    var failureSound = AVAudioPlayer()
    var quests: Results<Quest>!
    var notificationToken: NotificationToken?
    
    var quest = Quest()
    var subquests: [Quest] = []
    
    var playerResult: Results<Player>!
    var player: Player = Player()
    
    //MARK: - IBActions
    @IBAction func completeButtonClicked(sender: UIButton){
        switch quest.questType {
        case .dailyProgress:
            progressQuest()
        case .normal:
            completeQuest()
        }
    }
    
    @IBAction func abandonButtonClicked(sender: UIButton){
        let alert = UIAlertController(title: "Amandon quest", message: "You will loose \(quest.questEP/2) EXP", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Abandon", style: .default, handler: { action in
            self.abandonQuest()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    //MARK: - View Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //REALM
        let realm = RealmService.shared.realm
        quests = realm.objects(Quest.self)
        playerResult = realm.objects(Player.self)
        player = playerResult[0]
        notificationToken = realm.observe{ (notification, realm) in
            self.tableView.reloadData()
        }
        
        //TABLEVIEW DELEGATES
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "AllQuestsTVC", bundle: nil), forCellReuseIdentifier: "allQuestsTVC")

        //AUDIO
        do{
            gloriousSound = try AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: Bundle.main.path(forResource: "Questcomplete", ofType: "mp3")!))
            gloriousSound.prepareToPlay()
            failureSound = try AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: Bundle.main.path(forResource: "Questabandoned", ofType: "mp3")!))
            failureSound.prepareToPlay()
        }catch{
            print(error)
        }
        
        // OTHER
        reloadLayout(quest: quest)
        
        if quest.questDeadline != nil {
            let daysToDeadline = Int(Calendar.current.dateComponents([.day], from: Date(), to: quest.questDeadline!).day!)
            if daysToDeadline < 0{
                abandonQuest()
            }
        }
        
    }
    
    //MARK: - All of UI
    
    func prepareUI(){
        questCompletedView.isHidden = true
        questFinishedView.isHidden = true
        dailyProgressQuestTypeView.isHidden = true
        normalQuestTypeView.isHidden = true
        notCompletedViewBigQuestNameLabel.isHidden = true
        completeQuestButton.isEnabled = false
        completeQuestButton.alpha = 0.4
        expiriencePointsToGainLabel.alpha = 0.4
    }
    
    func reloadLayout(quest: Quest){
        prepareUI()
        
        questImportanceColor = quest.questImportance.color
        subquests = getSubquests(subquestsIDs: quest.subQuestIDs)
        
        questFinishedIcon.image = questFinishedIcon.image!.withRenderingMode(.alwaysTemplate)
        questFinishedIcon.tintColor = questImportanceColor
        questFinishedLabel.textColor = questImportanceColor
        
        self.navigationController?.navigationBar.tintColor = questImportanceColor
        
        finishedViewQuestNameLabel.text = quest.questName
        
        notCompletedViewQuestNameLabel.text = quest.questName
        notCompletedViewQuestNameLabel.textColor = questImportanceColor
        
        questDescriptionLabel.text = quest.questDescription
        
        completeQuestButton.backgroundColor = questImportanceColor
        
        expiriencePointsToLooseLabel.text = "\(quest.questImportance.expiriencePoints/2) EXP"
        expiriencePointsToGainLabel.text = "\(quest.questImportance.expiriencePoints) EXP"
        expiriencePointsToGainLabel.textColor = questImportanceColor
        
        progressLabel.text = "\(quest.questProgress) / \(quest.questProgressCap)"
        
        switch quest.questType {
        case .dailyProgress:
            completeQuestButton.alpha = 1
            expiriencePointsToGainLabel.alpha = 1
            dailyProgressQuestTypeView.isHidden = false
            if quest.questState == .readyToComplete{
                completeQuestButton.setTitle("Complete", for: .normal)
            }else{
                completeQuestButton.setTitle("Progress", for: .normal)
            }
        case .normal:
            normalQuestTypeView.isHidden = false
            completeQuestButton.setTitle("Complete", for: .normal)
            if quest.questDescription == "" && quest.subQuestIDs.isEmpty {
                notCompletedViewBigQuestNameLabel.text = quest.questName
                notCompletedViewBigQuestNameLabel.isHidden = false
                notCompletedViewBigQuestNameLabel.textColor = questImportanceColor
                notCompletedViewQuestNameLabel.isHidden = true
            }
        }
        
        let daysSinceLastProgress = Int(Calendar.current.dateComponents([.day], from: quest.lastProgress!, to: Date()).day!)
       
        switch quest.questState {
        case .notCompleted:
            if quest.questType == .dailyProgress {
                if daysSinceLastProgress == 1 || quest.questProgress == 0 {
                    completeQuestButton.isEnabled = true
                }else if daysSinceLastProgress < 1 {
                    completeQuestButton.alpha = 0.4
                    expiriencePointsToGainLabel.alpha = 0.4
                }else if daysSinceLastProgress > 1 {
                    abandonQuest()
                }
            }else{
                if quest.chainQuestIsReadyToComplete(subquests: subquests) {
                    showCompleteQuestButtonAndExpiriencePoints()
                }
            }
        case .readyToComplete:
            showCompleteQuestButtonAndExpiriencePoints()
        case .completed:
            finishedViewWithQuestImportanceColor()
            hideQuestNotCompletedView()
            finishedViewExpiriencePointsLabel.text = "+\(quest.questEP) EXP"
            questFinishStatusLabel.text = "Completed"
        case .abandoned:
            finishedViewWithAbandonColor()
            hideQuestNotCompletedView()
            finishedViewWithAbandonData()
        }
    }
    
    func questFinishAnimation(){
        
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseInOut, animations: {
            self.questNotCompletedView.alpha = 0
            
        }) { (finished) in
            self.questNotCompletedView.isHidden = true
            self.questCompleteAnimationParams(show: false)
            self.questCompletedView.isHidden = false
            UIView.animate(withDuration: 0.7, delay: 0, options: .curveEaseInOut, animations: {
               self.questCompleteAnimationParams(show: true)
            }, completion: { (finished) in
                UIView.animate(withDuration: 0.7, delay: 2, options: .curveEaseInOut, animations: {
                    self.questCompleteAnimationParams(show: false)
                }, completion: { (finished) in
                    UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseInOut, animations: {
                        self.questFinishedView.isHidden = false
                        self.questFinishedView.alpha = 1
                    }, completion: nil)
                })
                
            })
        }
    }
    
    func questCompleteAnimationParams(show: Bool){
        if show {
            self.questFinishedIcon.frame.origin.x += self.questFinishedIcon.frame.width
            self.questFinishedLabel.frame.origin.x -= self.questFinishedIcon.frame.width
            self.questFinishedIcon.alpha = 1
            self.questFinishedLabel.alpha = 1
        }else{
            self.questFinishedIcon.frame.origin.x -= self.questFinishedIcon.frame.width
            self.questFinishedLabel.frame.origin.x += self.questFinishedIcon.frame.width
            self.questFinishedIcon.alpha = 0
            self.questFinishedLabel.alpha = 0
        }
    }
    
    func abandonQuest(){
        //DATA
        quest.abandon()
        player.questAbandoned(quest: quest)
        
        //UI
        finishedViewWithAbandonColor()
        finishedViewWithAbandonData()
        questFinishedIcon.tintColor = Colors.abandonRed.tint
        questFinishedLabel.textColor = Colors.abandonRed.tint
        questFinishedLabel.text = "Quest Abandoned"
        questFinishAnimation()
        failureSound.play()
    }
    
    func progressQuest(){
        if quest.questState == .notCompleted{
            quest.progressQuest()
            reloadLayout(quest: quest)
        }else if quest.questState == .readyToComplete{
            quest.progressQuest()
            completeQuest()
        }
    }

    func completeQuest(){
        //DATA
        quest.complete()
        player.questCompleted(quest: quest)
        
        //UI
        finishedViewWithQuestImportanceColor()
        finishedViewExpiriencePointsLabel.text = "+\(quest.questEP) EXP"
        questFinishAnimation()
        gloriousSound.play()
    }
    
    func finishedViewWithQuestImportanceColor(){
        questFinishStatusLabel.textColor = questImportanceColor
        finishedViewQuestNameLabel.textColor = questImportanceColor
        finishedViewExpiriencePointsLabel.textColor = questImportanceColor
    }
    
    func finishedViewWithAbandonColor(){
        questFinishStatusLabel.textColor = Colors.abandonRed.tint
        finishedViewQuestNameLabel.textColor = Colors.abandonRed.tint
        finishedViewExpiriencePointsLabel.textColor = Colors.abandonRed.tint
        self.navigationController?.navigationBar.tintColor = Colors.abandonRed.tint
    }
    
    func finishedViewWithAbandonData(){
        finishedViewExpiriencePointsLabel.text = "-\(quest.questEP/2) EXP"
        questFinishStatusLabel.text = "Abandoned"
    }
    
    func showCompleteQuestButtonAndExpiriencePoints(){
        completeQuestButton.isEnabled = true
        completeQuestButton.alpha = 1
        expiriencePointsToGainLabel.alpha = 1
    }
    
    func hideQuestNotCompletedView(){
        questNotCompletedView.isHidden = true
        questFinishedView.isHidden = false
        questFinishedView.alpha = 1
    }
    // MARK: - TableView Data
    
    func getSubquests(subquestsIDs: List<String>) -> [Quest]{
        var subquests: [Quest] = []
        for ids in quest.subQuestIDs {
            let quest = quests.filter({$0.questID == ids}).first
            subquests.append(quest!)
        }
        return subquests
    }
    
//    func chainQuestIsReadyToComplete(subquests: [Quest]) -> Bool {
//        if !subquests.isEmpty {
//            for subquest in subquests {
//                if subquest.questState != .completed{
//                    return false
//                }
//            }
//            let dict: [String: Any?] = ["questState": QuestState.readyToComplete.rawValue]
//            RealmService.shared.update(self.quest, with: dict)
//            return true
//        }else{
//            return false
//        }
//    }
    
    // MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !subquests.isEmpty {
            return subquests.count
        }else{
            tableView.isHidden = true
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "allQuestsTVC", for: indexPath) as! AllQuestsTVC
        
        
        if !subquests.isEmpty {
            var questName: String
            var questColor: UIColor
            if subquests[indexPath.row].questState == .completed {
                questName = "[DONE] \(subquests[indexPath.row].questName)"
                questColor = subquests[indexPath.row].questImportance.color.withAlphaComponent(0.3)
            }else{
                questName = "\(subquests[indexPath.row].questName)"
                questColor = subquests[indexPath.row].questImportance.color
            }
             cell.confifure(name: questName, color: questColor)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Subquests"
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        quest = subquests[indexPath.row]
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
            self.questNotCompletedView.alpha = 0
        }, completion: { (finished) in
            self.reloadLayout(quest: self.quest)
            self.tableView.reloadData()
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
                self.questNotCompletedView.alpha = 1
            }, completion: nil)
        })
    }
}
