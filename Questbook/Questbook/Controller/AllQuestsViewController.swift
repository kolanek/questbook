//
//  AllQuestsViewController.swift
//  Questbook
//
//  Created by Bartosz Kolanek on 02.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import UIKit
import RealmSwift

class AllQuestsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var allQuests: Results<Quest>!
    var notificationToken: NotificationToken?
    var selectedQuest: Quest = Quest()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let realm = RealmService.shared.realm
        allQuests = realm.objects(Quest.self)
        notificationToken = realm.observe{ (notification, realm) in
            self.tableView.reloadData()
        }
//        for quest in allQuests{
//            RealmService.shared.delete(quest)
//        }
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "AllQuestsTVC", bundle: nil), forCellReuseIdentifier: "allQuestsTVC")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfRowsIn() -> [Int]{
        var secondaryQuests = 0
        var primaryQuests = 0
        var epicQuests = 0
        
        
        for quest in allQuests {
            switch quest.questImportance{
            case .secondary:
                secondaryQuests+=1
            case .primary:
                primaryQuests+=1
            case .epic:
                epicQuests+=1
            }
        }
        return [secondaryQuests, primaryQuests, epicQuests]
    }
    
    func notCompletedQuests() -> [[Quest]]{
        var epicQuests: [Quest] = []
        var primaryQuests: [Quest] = []
        var secondaryQuests: [Quest] = []
        
        for quest in allQuests{
            if quest.questState != .abandoned && quest.questState != .completed{
                switch quest.questImportance{
                case .secondary:
                    secondaryQuests.append(quest)
                case .primary:
                    primaryQuests.append(quest)
                case .epic:
                    epicQuests.append(quest)
                }
            }
        }
        return [secondaryQuests, primaryQuests, epicQuests]
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notCompletedQuests()[section].count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Secondary Quests"
        case 1:
            return "Primary Quests"
        case 2:
            return "Epic Quests"
        default:
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "allQuestsTVC", for: indexPath) as! AllQuestsTVC
        
        let color: UIColor
        
        switch indexPath.section {
        case 0:
            color = QuestImportance.secondary.color
        case 1:
            color = QuestImportance.primary.color
        case 2:
            color = QuestImportance.epic.color
        default:
            color = UIColor(red: 255, green: 255, blue: 255, alpha: 1)
        }
        
        cell.confifure(name: notCompletedQuests()[indexPath.section][indexPath.row].questName, color: color)
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedQuest = notCompletedQuests()[indexPath.section][indexPath.row]
        performSegue(withIdentifier: "goToQuest", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToQuest" {
            let secondVC: SingleQuestViewController = segue.destination as! SingleQuestViewController
            secondVC.quest = selectedQuest
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
